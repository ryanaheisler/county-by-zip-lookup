# CountyByZipLookup
Looks in TSV files for Zip Codes, finds the county for that zip code, and adds the county and State Abbreviation to the TSV

us_postal_codes.csv contains Zip Code, Place Name, State, State Abbreviation, County, Latitude, and Longitude for all Zip Codes in the United States.
