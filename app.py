import os

# Get files to process - CSVs - from working directory

filesToProcess = []

path, dirnames, filenames = os.walk(os.path.join(os.path.expanduser('~'),'Desktop')).next()

for filename in filenames:
    if '.tsv' in filename[-4:]:
        filesToProcess.append(os.path.join(path, filename))

# Create dictionary of zip codes to their state abbreviations and county names

with open('counties_zips_and_abbreviations.csv', 'r') as file_:

    lines = file_.readlines()

abbreviations_counties_by_zip = {}

for line in lines:

    values = line.strip().split(',')

    abbreviations_counties_by_zip[values[1]] = [values[0], values[2]]

# Process files

for fileToProcess in filesToProcess:

    with open(fileToProcess, 'r') as file_:

        lines = file_.readlines()

    # Find which column has the zip code

    headerValues = lines[0].strip().split('\t')
    zipColumnIndex = -1

    for i in range(len(headerValues)):

        if 'zip' in headerValues[i].lower():

            zipColumnIndex = i

    if zipColumnIndex == -1:

        print 'File %s does not contain a column with zip codes' % fileToProcess[2:]

    # Make a list of lists:
    # First index contains the header values from the original file plus columns for state abbreviation and county name

    else:
        newLineValues = []
        newValues = []

        if zipColumnIndex > 0:

            for i in range(zipColumnIndex):

                newValues.append(headerValues[i].strip())

        newValues.append('State Abbreviation')
        newValues.append(headerValues[zipColumnIndex].strip())
        newValues.append('County')

        for i in range(zipColumnIndex + 1, len(headerValues)):
            newValues.append(headerValues[i].strip())

        newLineValues.append(newValues)

        # All subsequent indices contain values from the original file plus columns for state abbreviation and county

        for line in lines[1:]:

            columnValues = line.strip().split('\t')
            newValues = []

            fullZipCode = columnValues[zipColumnIndex].strip()
            fiveDigitZip = fullZipCode[:5]

            # If line contains non-numeric characters in the 5-digit zip code, don't try to use it as a key in the dict

            if fiveDigitZip.isdigit():

                abbreviation_and_county_entry = abbreviations_counties_by_zip[fiveDigitZip]

            else:

                alphaCharMessage = "ZIP CODE CONTAINS ALPHA CHARACTERS"
                abbreviation_and_county_entry = [alphaCharMessage, alphaCharMessage]

            if zipColumnIndex > 0:

                for i in range(zipColumnIndex):
                    newValues.append(columnValues[i].strip())

            newValues.append(abbreviation_and_county_entry[0])
            newValues.append(fullZipCode)
            newValues.append(abbreviation_and_county_entry[1])

            for i in range(zipColumnIndex + 1, len(columnValues)):
                newValues.append(columnValues[i].strip())

            newLineValues.append(newValues)

        # join each list into a string to write to the new file

        linesToWrite = []

        for line in newLineValues:

            linesToWrite.append('\t'.join(line) + os.linesep)

        with open(fileToProcess, 'w') as file_:

            file_.writelines(linesToWrite)
