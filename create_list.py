with open('./us_postal_codes.csv', 'r') as file_:

    lines = file_.readlines()

zipsAbbreviationsAndCounties = []

for line in lines[:]:

    columnValues = line.split(',')

    while len(columnValues[0]) < 5:

        columnValues[0] = '0' + columnValues[0]

    zipsAbbreviationsAndCounties.append([columnValues[i] for i in 3, 0, 4])

with open('counties_zips_and_abbreviations.csv', 'w+') as file_:

    for list_ in zipsAbbreviationsAndCounties:

        file_.write('%s,%s,%s\r\n' % (list_[0], list_[1], list_[2]))

